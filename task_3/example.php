<?php
/*
 * данный цикл выбирает во временную таблицу
 *  айдихи юзеров из таблицы user_accounts
 *  с непустым значением поля value вяжется с таблицей users по ключу
 *  u.id=a.id_user только те которым не проставлено поле count_accounts в users
 * берется 100 штук.
 * и апдейтся поле count_accounts в таблицу users тем у которых есть аккаунты
 * и грохается временная таблица
 * таким образом кусками апдейтится таблица users, с проверкой на выход из цикла
 *
 */
for($i = 0; $i<10000000; $i++) {
    $result = DB::query('
		CREATE TEMPORARY TABLE tmp1 AS
		SELECT a.id_user
		FROM user_accounts AS a
		INNER JOIN users AS u ON u.id=a.id_user
		WHERE (u.count_accounts IS NULL OR u.count_accounts=0)
        AND (a.value IS NOT NULL AND a.value<>0)
		LIMIT 100');
    $result_count = DB::query('SELECT COUNT(*) INTO return_count FROM tmp1', 'one');
    $result = DB::query('
		UPDATE users AS u
		SET count_accounts=1
		FROM (
			SELECT  id_user FROM tmp1
		) AS t
		WHERE u.id=t.id_user;');
    $result = DB::query('DROP TABLE tmp1');
    if ($result_count < 100) break;
}
/**
 * возможно можно обойтись без временной таблицы
 * использую 1 и тот же запрос для каунта и апдейта
 *
 */
$q = 'SELECT a.id_user
            FROM user_accounts AS a
            INNER JOIN users AS u ON u.id=a.id_user
            WHERE (u.count_accounts IS NULL OR u.count_accounts=0)
            AND (a.value IS NOT NULL AND a.value<>0) LIMIT 100';

for($i = 0; $i<10000000; $i++) {
    $result_count = DB::query("SELECT COUNT(*) INTO return_count FROM ($q)", 'one');
    $result = DB::query("
		UPDATE users AS U
		SET count_accounts=1
		FROM ($q) AS t
		WHERE U.id=t.id_user;");

    if ($result_count < 100) break;

}