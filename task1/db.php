<?php
class DB
{
    private $_db;
    static $_instance;

    private function __construct()
    {
        include 'config.php';
        try{
            $this->_db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
            $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (Exception $e){
            print_r($e->getMessage());
            exit;
        }
    }

    private function __clone(){}
    private function __wakeup(){}

    public static function getInstance()
    {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __call($method, $args)
    {
        if ( is_callable(array($this->_db, $method)) ) {
            return call_user_func_array(array($this->_db, $method), $args);
        }
        else {
            throw new BadMethodCallException('Undefined method Database::' . $method);
        }
    }
}