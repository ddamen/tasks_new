

CREATE TABLE `event_links` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`title`  varchar(250) NULL ,
`event_url`  varchar(250) NULL ,
PRIMARY KEY (`id`)
)

CREATE TABLE `calendar_messages` (
`id`  int(11) NOT NULL  AUTO_INCREMENT ,
`title`  varchar(250) NOT NULL ,
`message`  varchar(250) NOT NULL ,
`start`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`end`  timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY (`id`)