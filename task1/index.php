<?php
session_start();
require_once './vendor/autoload.php';
require_once 'db.php';
require_once 'GCalendar.php';

$client = new Google_Client();
$client->setAuthConfig('client_secret.json');
$redirect_uri = 'http://testhost.com/task1/index.php';
$client->setRedirectUri($redirect_uri);
$client->addScope(Google_Service_Calendar::CALENDAR);

if(isset($_SESSION['token'])){
    $client->setAccessToken($_SESSION['token']);
}
else{
    if (isset($_GET['code'])) {
        $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
        $client->setAccessToken($token);
        $_SESSION['token'] = $token;
    }
    else{
        $authUrl = $client->createAuthUrl();
        header("Location: " . "$authUrl");
    }
}


$googleCalendar = new GCalendar($client);
$title = 'TEst Title';
$desription = 'Call somewhere';
$start = '2017-05-16T09:00:00';
$end = '2017-05-16T09:20:00';
var_dump($googleCalendar->AddEvent($title, $desription, $start, $end));
exit;
