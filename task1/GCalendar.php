<?php
//require_once './vendor/autoload.php';

class GCalendar
{
    private $service;
    public $calendarId = 'primary';
    public $timeZone = 'Europe/Kiev';
    private $db;

    /**
     * GCalendar constructor.
     * @param Google_Client $client
     */
    public function __construct(Google_Client $client)
    {
        $this->service = new Google_Service_Calendar($client);
        $this->db = DB::getInstance();
    }

    /**
     * @param $title
     * @param $description
     * @param $start
     * @param $end
     * @return bool
     */
    public function AddEvent($title, $description, $start, $end)
    {
        if($this->checkExist($title, $description))
            return false;

        $event = new Google_Service_Calendar_Event([
            'summary' => $title,
            'description' => $description,
            'start' => [
                'dateTime' => $start,
                'timeZone' => $this->timeZone,
            ],
            'end' => [
                'dateTime' => $end,
                'timeZone' => $this->timeZone,
            ],
        ]);
        $this->db->beginTransaction();
        try{
            $event = $this->service->events->insert($this->calendarId, $event);

            $sql =  $this->db->prepare("INSERT INTO `calendar_messages` (title, description, start, `end`)
                    VALUES (:title, :description, :start, :end)");

            $sql->execute([
                'title' => $title,
                'description' => $description,
                'start' => $start,
                'end' => $end
            ]);

            $sql = $this->db->prepare("INSERT INTO `event_links` (title, event_url)
                    VALUES (:title, :event_url)");

            $sql->execute([
                'title' => $title,
                'event_url' => $event->htmlLink
            ]);
        }
        catch (Exception $ex){
            $this->db->rollback();
            print_r($ex->getMessage());exit;
        }
        $this->db->commit();
        return true;
    }

    /**
     * Check if this event already exist in google Calendar
     * @param $title
     * @param $description
     * @return bool
     */
    public function checkExist($title, $description)
    {
        $events = $this->getEvents();
        if(!$events)
            return false;

        foreach ($events as $event){
            if($event->summary === $title && $event->description === $description)
                return true;
        }

        return false;
    }

    /**
     * return events from google Calendar
     * @return bool|mixed
     */
    public function getEvents()
    {
        $optParams = array(
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => TRUE,
            'timeMin' => date('c'),
        );

        $results = $this->service->events->listEvents($this->calendarId, $optParams);
        if(count($results->getItems())) {
            return $results->getItems();
        }

        return false;
    }
}