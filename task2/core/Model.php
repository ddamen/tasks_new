<?php
namespace Core;

class Model
{
    protected $table = 'pages';
    private $db;

    public function __construct()
    {
        $this->db = DB::getInstance();
    }

    public function getPage($friendly)
    {
        $friendly = $this->escapeString($friendly);
        $statement = "SELECT * FROM `$this->table` WHERE friendly = $friendly";

        $response = [];
        if($result = $this->execute($statement)){
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                $response[] = $row;
            }
        }

        return $response;

    }
    public function execute($statement)
    {
        return  $this->db->query($statement);
    }

    public function escapeString($string)
    {
        return $this->db->quote($string);
    }
}