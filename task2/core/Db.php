<?php
namespace Core;

class DB
{
    private $_db;
    static $_instance;

    private function __construct() {
        try{
            $this->_db = new \PDO("mysql:host=" . DB_HOST .";dbname=". DB_NAME. '"', DB_USER, DB_PASS);
            $this->_db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        }
        catch (\Exception $e){
            print_r($e->getMessage());
            exit;
        }
    }

    private function __clone(){}
    public function __wakeup(){}
    
    public static function getInstance() {
        if (!(self::$_instance instanceof self)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function __call ($method, $args) {
        if ( is_callable(array($this->_db, $method)) ) {
            return call_user_func_array(array($this->_db, $method), $args);
        }
        else {
            throw new \BadMethodCallException('Undefined method Database::' . $method);
        }
    }
}