<?php
namespace Core;

class View
{
    public $viewName;
    public $data;

    public function __construct($data, $view)
    {
        $this->viewName = $view;
        $this->data = $data;
    }

    public function render()
    {
        $title =  $this->data['title'];
        $description = $this->data['description'];
        include('app/views/' .$this->viewName . '.php');
    }

}