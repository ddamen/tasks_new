<?php
namespace Core;

class Controller
{
    protected $model;
    protected $view;

    public function __construct($friendly = '')
    {
        $this->model = new Model();
        $data = $this->model->getPage($friendly);

        if(empty($data))
           Route::show404();

        $this->view = new View($data[0], 'index');
    }

    public function index()
    {
       $this->view->render();
    }
}