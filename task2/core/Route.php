<?php
namespace Core;

class Route
{
    public static function run()
    {
        $route = explode('?', $_SERVER['REQUEST_URI']);
        $controller = new Controller($route[0]);
        return $controller->index();
    }

    public static function show404()
    {
        header("HTTP/1.0 404 Not Found");
        include 'app/views/404.php';
        exit;

    }
}