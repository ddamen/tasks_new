<?php
error_reporting(E_ALL);
ini_set("display_errors", 1);

include __DIR__ . '/app/configs/db.php';

define('DB_HOST', $db_host);
define('DB_NAME', $db_name);
define('DB_USER', $db_user);
define('DB_PASS', $db_pass);

Route::run();


